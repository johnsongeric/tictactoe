#pragma once
#include <iostream>
#include <string>
using namespace std;

class TicTacToe
{
private:
	char m_board[9];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;

public:
	
	// Constructor
	TicTacToe()
	{
		
		// Init all the fields

		// Compiler seems to yell at me unless I init the board array like this
		// I feel like there is a better way to do this, but I'm moving on for now.
		m_board[0] = ' ';
		m_board[1] = ' ';
		m_board[2] = ' ';
		m_board[3] = ' ';
		m_board[4] = ' ';
		m_board[5] = ' ';
		m_board[6] = ' ';
		m_board[7] = ' ';
		m_board[8] = ' ';
		
		m_numTurns = 0;
		m_playerTurn = 'Y'; // Set to Y because the swap in IsOver(), and its called before the game even begins.
		m_winner = ' ';
	}
	// mutator
	
	// accessor



	// Other Methods

	void DisplayBoard()
	{
		// Display the game board
		cout << m_board[0] << "|" << m_board[1] << "|" << m_board[2] << "\n";
		cout << "- - -\n";
		cout << m_board[3] << "|" << m_board[4] << "|" << m_board[5] << "\n";
		cout << "- - -\n";
		cout << m_board[6] << "|" << m_board[7] << "|" << m_board[8] << "\n";

	}
	bool IsOver()
	{
		// Is the game over?
		
		// Rows
		// Top Row is all the same, and not blank
		if ((m_board[0] == 'X' || m_board[0] == 'Y') && m_board[0] == m_board[1] && m_board[1] == m_board[2])
		{
			m_winner = GetPlayerTurn();
			return true;
		}
		// Mid Row is all the same, and not blank
		if ((m_board[3] == 'X' || m_board[3] == 'Y') && m_board[3] == m_board[4] && m_board[4] == m_board[5])
		{
			m_winner = GetPlayerTurn();
			return true;
		}
		// Bottom Row is all the same, and not blank
		if ((m_board[6] == 'X' || m_board[6] == 'Y') && m_board[6] == m_board[7] && m_board[7] == m_board[8])
		{
			m_winner = GetPlayerTurn();
			return true;
		}

		// Col
		// left col is all the same, and not blank
		if ((m_board[0] == 'X' || m_board[0] == 'Y') && m_board[0] == m_board[3] && m_board[3] == m_board[6])
		{
			m_winner = GetPlayerTurn();
			return true;
		}
		// Mid col is all the same, and not blank
		if ((m_board[1] == 'X' || m_board[1] == 'Y') && m_board[1] == m_board[4] && m_board[4] == m_board[7])
		{
			m_winner = GetPlayerTurn();
			return true;
		}
		// Right col is all the same, and not blank
		if ((m_board[2] == 'X' || m_board[2] == 'Y') && m_board[2] == m_board[5] && m_board[5] == m_board[8])
		{
			m_winner = GetPlayerTurn();
			return true;
		}

		// Diag
		// backslash is all the same, and not blank
		if ((m_board[0] == 'X' || m_board[0] == 'Y') && m_board[0] == m_board[4] && m_board[4] == m_board[8])
		{
			m_winner = GetPlayerTurn();
			return true;
		}
		// Forslash is all the same, and not blank
		if ((m_board[2] == 'X' || m_board[2] == 'Y') && m_board[2] == m_board[4] && m_board[4] == m_board[6])
		{
			m_winner = GetPlayerTurn();
			return true;
		}

		// swap the current player
		if (GetPlayerTurn() == 'X')
		{
			m_playerTurn = 'Y';
		}
		else
		{
			m_playerTurn = 'X';
		}

		// Has all the moves been taken? if so game is cat
		if (m_numTurns == 9)
		{
			m_winner = 'C';
			return true;
		}
		// If it gets to here, no winning moves have been played, and there are still more turns to go.
		return false;
	}
	char GetPlayerTurn()
	{
		return m_playerTurn;
	}
	bool IsValidMove(int pos)
	{
		// Check if the space is empty and valid
		if (m_board[pos-1] == ' ')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	void Move(int pos)
	{
		// Get the current player and place the token on the board
		m_board[pos - 1] = GetPlayerTurn();
		// incriment num moves
		m_numTurns++;
	}
	void DisplayResult()
	{
		//check for cat game
		if (m_winner == 'C')
		{
			cout << "Cat game! It's a draw! ~^_^~\n";
		}
		else
		{
			cout << m_winner << " Wins the game!\n";
		}
	}

};